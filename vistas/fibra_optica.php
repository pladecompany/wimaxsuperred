<section class="hero-section inner-page">
	<div class="wave">
		<svg width="1920px" height="265px" viewBox="0 0 1920 265" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
					<path d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,667 L1017.15166,667 L0,667 L0,439.134243 Z" id="Path"></path>
				</g>
			</g>
		</svg>
	</div>

	<div class="container">
		<div class="row align-items-center">
			<div class="col-12">
				<div class="row justify-content-center">
					<div class="col-md-7 text-center hero-text">
						<h1 data-aos="fade-up" data-aos-delay="">Conoce nuestro servicio de Fibra Óptica</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section">
	<div class="container">
		<div class="row justify-content-center text-center mb-5">
			<div class="col-md-5" data-aos="fade-up">
				<h2 class="section-heading">Beneficios</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 text-center " data-aos="fade-up" data-aos-delay="200">
				<div class="feature-1 text-center">
					<div class="wrap-icon icon-1 flex flex-center item-center ">
						<span class="icofont-worker icon "></span>
					</div>
				</div>
				<p>Fácil de instalar.</p>
			</div>

			<div class="col-md-4 text-center " data-aos="fade-up" data-aos-delay="200">
				<div class="feature-1 text-center">
					<div class="wrap-icon icon-1  flex flex-center item-center ">
						<span class="icofont-fast-delivery icon"></span>
					</div>
				</div>
				<p>Transmisión de datos a alta velocidad.</p>
			</div>

			<div class="col-md-4 text-center " data-aos="fade-up" data-aos-delay="200">
				<div class="feature-1 text-center">
					<div class="wrap-icon icon-1  flex flex-center item-center  ">
						<span class="icofont-clock-time icon"></span>
					</div>
				</div>
				<p> Acceso ilimitado y continuo las 24 horas del día, sin congestiones.</p>
			</div>

			<div class="col-md-4 text-center " data-aos="fade-up" data-aos-delay="200">
				<div class="feature-1 text-center">
					<div class="wrap-icon icon-1  flex flex-center item-center  ">
						<span class="icofont-telephone icon"></span>
					</div>
				</div>
				<p>Insensibilidad a la interferencia electromagnética, como ocurre cuando un alambre telefónico pierde parte de su señal.</p>
			</div>

			<div class="col-md-4 text-center " data-aos="fade-up" data-aos-delay="200">
				<div class="feature-1 text-center">
					<div class="wrap-icon icon-1  flex flex-center item-center  ">
						<span class="icofont-power-zone icon"></span>
					</div>
				</div>
				<p>Resistencia al calor, frío y a la corrosión.</p>
			</div>

			<div class="col-md-4 text-center " data-aos="fade-up" data-aos-delay="200">
				<div class="feature-1 text-center">
					<div class="wrap-icon icon-1  flex flex-center item-center  ">
						<span class="icofont-architecture-alt icon"></span>
					</div>
				</div>
				<p>Compatibilidad con la tecnología digital.</p>
			</div>
		</div>

		<div class="row pt-5 mt-5 flex flex-center">
			<div class="col-md-8 mb-5 mb-md-0 " data-aos="fade-up">
				<form action="forms/contact.php" method="post" role="form" class="php-email-form">
					<div class="text-center mb-4 ">
						<h2>¿Estás interesado en <b>fibra óptica?</b> </h2>
					</div>

					<p class="">Desde <b>Wimax Super Red</b> estamos realizando este registro para captar a las personas interesadas en el servicio, su información será almacenada en nuestra base de datos y nos pondremos en contacto para confirmar su interés en <b>nuestro servicio</b>.</p>

					<p class="">Este registro será utilizado como un censo para determinar cuáles son las zonas con más demanda del servicio y en base a esto daremos prioridad a las zonas</p>

					<div class="row flex flex-center ">
						<div class="col-md-8  mt-3 form-group">
							<label for="name">Nombre y apellido </label>
							<input type="text" name="name" class="form-control" id="name" data-rule="minlen:4" data-msg="Escribe tu nombre" />
							<div class="validate"></div>
						</div>

						<div class="col-md-8 form-group mt-3 ">
							<label for="name">Correo electrónico</label>
							<input type="email" class="form-control" name="email" id="email" data-rule="email" data-msg="Escribe un correo válido" />
							<div class="validate"></div>
						</div>

						<div class="col-md-8 form-group mt-3 ">
							<label for="name">Télefono/Whatsapp</label>
							<input type="number" class="form-control" name="email" id="email" data-rule="email" data-msg="Escribe un correo válido" />
							<div class="validate"></div>
						</div>
						
						<div class="col-md-8  form-group mt-3">
							<label for="name">Dirección</label>
							<textarea class="form-control" placeholder="Ejemplo: urbanización/barrio/ Calle/avenida y casa" name="message" data-rule="required" data-msg="Escribe el mensaje que deseas comunicar"></textarea>
							<div class="validate"></div>
						</div>

						<div class="col-md-8 mb-3">
							<div class="loading">Cargando</div>
							<div class="error-message"></div>
							<div class="sent-message">Tu mensaje ha sido enviado, gracias por comunicarte</div>
						</div>

						<div class="col-md-12 flex flex-center form-group">
							<input type="submit" class="btn btn-primary d-block w-50" value="Enviar">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

