<section class="hero-section" id="hero">
	<div class="wave">
		<svg width="100%" height="355px" viewBox="0 0 1920 355" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
					<path d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,757 L1017.15166,757 L0,757 L0,439.134243 Z" id="Path"></path>
				</g>
			</g>
		</svg>
	</div>

	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 hero-text-image">
				<div class="row">
					<div class="col-lg-8 text-center text-lg-start">
						<h1 data-aos="fade-right">SOMOS INNOVACIÓN Y TECNOLOGÍA A TU ALCANCE</h1>
						<p class="mb-5" data-aos="fade-right" data-aos-delay="100">Conectividad y seguridad para hogares y empresas</p>
						<p data-aos="fade-right" data-aos-delay="200" data-aos-offset="-500"><a href="index.php?op=servicios" class="btn btn-outline-white">Únete a la Super Red</a></p>
					</div>
					<div class="col-lg-4 iphone-wrap">
						<img src="static/img/phone_1.png" alt="Image" class="phone-1" data-aos="fade-right">
						<img src="static/img/phone_2.png" alt="Image" class="phone-2" data-aos="fade-right" data-aos-delay="200">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section">
	<div class="container">
		<div class="row justify-content-center text-center mb-5">
			<div class="col-md-5" data-aos="fade-up">
				<h2 class="section-heading">¿Por qué elegir Wimax Super Red?</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 text-center " data-aos="fade-up" data-aos-delay="">
				<div class="feature-1 text-center">
					<div class="wrap-icon icon-1 flex flex-center item-center ">
						<span class="icofont-thumbs-up icon "></span>
					</div>
					<h3 class="mb-3">Integridad</h3>
					
				</div>
				<p>Estamos comprometemos con la satisfacción de nuestros clientes.</p>
			</div>
			<div class="col-md-4 text-center " data-aos="fade-up" data-aos-delay="100">
				<div class="feature-1 text-center">
					<div class="wrap-icon icon-1 flex flex-center item-center ">
						<span class="icofont-tools-bag icon "></span>
					</div>
					<h3 class="mb-3">Servicio</h3>
				
				</div>
				<p>Contamos con un personal altamente calificado el cual está disponible para tí y tus necesidades.</p>
			</div>
			<div class="col-md-4 text-center " data-aos="fade-up" data-aos-delay="200">
				<div class="feature-1 text-center">
					<div class="wrap-icon icon-1 flex flex-center item-center ">
						<span class="icofont-computer  icon "></span>
					</div>
					<h3 class="mb-3">Vanguardia tecnológica</h3>
					
				</div>
				<p>Es un placer contar con las últimas tecnologías para tí.</p>
			</div>

			
		</div>
	</div>
</section>

<section class="section cta-section">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-8 offset-md-2 me-auto text-center text-md-start mb-3 mb-md-0">
				<h2 class="text-center">¿Estás interesado en nuestro internet con fibra óptica?</h2>
				<p class="text-center"><a href="index.php?op=fibra_optica" target="_blank" class="btn"><span class="icofont-red"></span> Solicítalo aquí</a></p>
			</div>
		</div>
	</div>
</section>

<section id="nosotros" class="section">
	<div class="container">
		<div class="row align-items-center">
			

			<div class="col-md-6" data-aos="fade-right">
				<img src="static/img/team.jpg" alt="Image" class="img-fluid">
			</div>

			<div class="col-md-4 ms-auto">

				<h2 class="mb-4">¡Estamos para apoyarte!</h2>
				<p class="mb-4">Contamos con el soporte humano, que nos permite ofrecer la <b>garantía, confiabilidad y seguridad</b> de nuestros servicios. Visítenos y obtendrá la mejor atención por parte de nuestro equipo altamente capacitado, dispuesto a atender tus solicitudes o dudas del día a día.</p>

				<div class="row">
					<div class="col-md-12">
						<div class="step">
							<h5><i class="icofont-check-circled"></i> Técnicos</h5>
						</div>
					</div>
					<div class="col-md-12">
						<div class="step">
							<h5><i class="icofont-check-circled"></i> Administradores</h5>
						</div>
					</div>
					<div class="col-md-12">
						<div class="step">
							<h5><i class="icofont-check-circled"></i> Atención al cliente</h5>
						</div>
					</div>
				</div>
			</div>

            <div class="col-md-12 mt-5 text-center ">

			<h2 class="mb-4">¡Y estamos siempre presente en la comunidad!</h2>

			<div class="main-carousel mb-5 " data-flickity='{ "cellAlign": "left", "contain": true , "wrapAround": true , "freeScroll": true, "groupCells": true }'>

			<div class=" cell ">
				<a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/equipo/IMG-8.jpg" class="img-equipo" alt="Image" class="img-fluid">
				</a>
				<!-- <h5><a href="#" class="text-white">Circuito cerrado CCTV</a></h5> -->
			</div>

			<div class=" cell ">
				<a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/equipo/IMG-13.jpg" class="img-equipo" alt="Image" class="img-fluid">
				</a>
				<!-- <h5><a href="#" class="text-white">Cercado eléctrico</a></h5> -->
			</div>

			<div class=" cell ">
		        <a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/equipo/IMG-20.jpg" class="img-equipo" alt="Image" class="img-fluid">
				</a>
				<!-- <h5><a href="#" class="text-white">Mantenimiento preventivo y correctivo de computadoras</a></h5> -->
			</div>

			<div class=" cell ">
		        <a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/equipo/IMG-14.jpg" class="img-equipo" alt="Image" class="img-fluid">
				</a>
				<!-- <h5><a href="#" class="text-white">Servicio de internet</a></h5> -->
			</div>

			<div class=" cell ">
		        <a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/equipo/IMG-15.jpg" class="img-equipo" alt="Image" class="img-fluid">
				</a>
				<!-- <h5><a href="#" class="text-white">Instalación de alarmas</a></h5> -->
			</div>


			<div class=" cell ">
		        <a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/equipo/IMG-19.jpg" class="img-equipo" alt="Image" class="img-fluid">
				</a>
				<!-- <h5><a href="#" class="text-white">Portones eléctricos</a></h5> -->
			</div>

			<div class=" cell ">
		        <a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/equipo/IMG-16.jpg" class="img-equipo" alt="Image" class="img-fluid">
				</a>
				<!-- <h5><a href="#" class="text-white">Portones eléctricos</a></h5> -->
			</div>

			<div class=" cell ">
		        <a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/equipo/IMG-17.jpg" class="img-equipo" alt="Image" class="img-fluid">
				</a>
				<!-- <h5><a href="#" class="text-white">Mantenimiento preventivo y correctivo de computadoras</a></h5> -->
			</div>

		</div>
			
			</div>
			
		</div>
	</div>
</section>

<section id="servicios" class="section" style="background: #0074ba;">
	<div class="container pl-3 pr-3 ">
		<div class="row justify-content-center text-center">
			<div class="col-md-7 mb-5">
				<h2 class="section-heading-b">Nuestros servicios</h2>
				<p class="text-white">Ofrecemos gran variedad de servicios tecnológicos y de seguridad.</p>
			</div>
		</div>

		<div class="main-carousel mb-5 " data-flickity='{ "cellAlign": "left", "contain": true , "wrapAround": true , "freeScroll": true, "groupCells": true }'>

			<div class=" cell ">
				<a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/serv_camaras.jpg" class="img-servicios" alt="Image" class="img-fluid">
				</a>
				<h5><a href="#" class="text-white">Circuito cerrado CCTV</a></h5>
			</div>

			<div class=" cell ">
				<a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/serv_cercoelectrico.jpg"  class="img-servicios" alt="Image" class="img-fluid">
				</a>
				<h5><a href="#" class="text-white">Cercado eléctrico</a></h5>
			</div>

			<div class=" cell ">
		        <a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/serv_internet.jpg"  class="img-servicios" alt="Image" class="img-fluid">
				</a>
				<h5><a href="#" class="text-white">Servicio de internet</a></h5>
			</div>

			<div class=" cell ">
		        <a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/serv_alarmas.jpg"  class="img-servicios" alt="Image" class="img-fluid">
				</a>
				<h5><a href="#" class="text-white">Instalación de alarmas</a></h5>
			</div>

			<div class=" cell ">
		        <a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/serv_portones.jpg"  class="img-servicios" alt="Image" class="img-fluid">
				</a>
				<h5><a href="#" class="text-white">Portones eléctricos</a></h5>
			</div>
<!--
			<div class=" cell ">
			        <a href="#" class="img-servicio d-block mb-4">
						<img src="static/img/serv_motor.jpg"  class="img-servicios" alt="Image" class="img-fluid">
					</a>
					<h5><a href="#" class="text-white">Instalación/mantenimiento motor</a></h5>
			</div>

			<div class=" cell ">
			        <a href="#" class="img-servicio d-block mb-4">
						<img src="static/img/serv_controles.jpg"  class="img-servicios" alt="Image" class="img-fluid">
					</a>
					<h5><a href="#" class="text-white">Controles/modelos varios</a></h5>
			</div>
-->
			<div class=" cell ">
		        <a href="#" class="img-servicio d-block mb-4">
					<img src="static/img/serv_mantenimiento.jpg"  class="img-servicios" alt="Image" class="img-fluid">
				</a>
				<h5><a href="#" class="text-white">Mantenimiento preventivo y correctivo de computadoras</a></h5>
			</div>

		</div>

		<div class="row">
			 <div class="col-lg-12 text-center">
				<p><a href="index.php?op=servicios" class="btn btn-primary">Ver todos</a></p>
			</div>
		</div>
	</div>
</section>

<section class="section border-top border-bottom" id="testimonios">
	<div class="container">
		<div class="row justify-content-center text-center mb-5">
			<div class="col-md-4">
				<h2 class="section-heading">Testimonios</h2>
			</div>
		</div>
		<div class="row justify-content-center text-center">
			<div class="col-md-7">
				<div class="owl-carousel testimonial-carousel">
					<div class="review text-center">
						<p class="stars">
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star muted"></span>
						</p>
						<h3>Muy buena atención y reciptividad con el internet, su atención es muy amena.</h3>

						<p class="review-user">
							<img src="static/img/user.png" alt="Image" class="img-fluid rounded-circle mb-3">
							<span class="d-block">
								<span class="text-black">Admin Bonanza</span>, &mdash; Cliente
							</span>
						</p>
					</div>

					<div class="review text-center">
						<p class="stars">
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star muted"></span>
						</p>
						<h3>Que internet más bueno ¡Me encanta!</h3>

						<p class="review-user">
							<img src="static/img/user.png" alt="Image" class="img-fluid rounded-circle mb-3">
							<span class="d-block">
								<span class="text-black">Karlita</span>, &mdash; Cliente
							</span>
						</p>
					</div>

					<div class="review text-center">
						<p class="stars">
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star muted"></span>
						</p>
						<h3>Excelente servicio y hermosa atención</h3>

						<p class="review-user">
							<img src="static/img/user.png" alt="Image" class="img-fluid rounded-circle mb-3">
							<span class="d-block">
								<span class="text-black">Elimar Lara</span>, &mdash; Cliente
							</span>
						</p>
					</div>

					<div class="review text-center">
						<p class="stars">
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star"></span>
							<span class="icofont-star muted"></span>
						</p>
						<h3>Excelente servicio</h3>

						<p class="review-user">
							<img src="static/img/user.png" alt="Image" class="img-fluid rounded-circle mb-3">
							<span class="d-block">
								<span class="text-black">Daniel Rodríguez</span>, &mdash; Cliente
							</span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
