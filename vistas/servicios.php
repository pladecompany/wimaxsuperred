<section class="hero-section inner-page">
	<div class="wave">
		<svg width="1920px" height="265px" viewBox="0 0 1920 265" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
				<path d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,667 L1017.15166,667 L0,667 L0,439.134243 Z" id="Path"></path>
				</g>
			</g>
		</svg>
	</div>

	<div class="container">
		<div class="row align-items-center">
			<div class="col-12">
				<div class="row justify-content-center">
				<div class="col-md-7 text-center hero-text">
					<h1 data-aos="fade-up" data-aos-delay="">Nuestros servicios</h1>
					<p class="mb-5" data-aos="fade-up" data-aos-delay="100">Ofrecemos gran variedad de servicios tecnológicos y de seguridad.</p>
				</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section">
	<div class="container">
		<div class="row mb-5">
			<div class="col-md-4">
				<div class="post-entry">
					<a href="#" class="img-servicio d-block mb-4">
						<img src="static/img/serv_camaras.jpg" class="img-servicios-info" alt="Image" class="img-fluid">
					</a>
					<div class="post-text">
						<h3>Circuito cerrado CCTV</h3>
						<p>Suministro e instalación de equipos conectados que generan imágenes en alta calidad para la seguridad del usuario.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="post-entry">
					<a href="#" class="img-servicio d-block mb-4">
						<img src="static/img/serv_cercoelectrico.jpg" class="img-servicios-info" alt="Image" class="img-fluid">
					</a>
					<div class="post-text">
						<h3>Cerco eléctrico</h3>
						<p>Suministro e instalación, seguridad para su hogar y/o empresa</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="post-entry">
					<a href="#" class="img-servicio d-block mb-4">
						<img src="static/img/serv_internet.jpg" alt="Image" class="img-servicios-info" class="img-fluid">
					</a>
					<div class="post-text">
						<h3>Servicio de internet</h3>
						<p>Variedad de planes de Internet, para que navegues a la velocidad que gustes, las 24 horas sin interrupciones.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="post-entry">
					<a href="#" class="img-servicio d-block mb-4">
						<img src="static/img/serv_alarmas.jpg" alt="Image" class="img-servicios-info" class="img-fluid">
					</a>
					<div class="post-text">
						<h3>Instalación de alarma</h3>
						<p>Sistema de alarma para tu vivienda o negocio impenetrable, 24 horas de protección personal y/o de bienes.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="post-entry">
					<a href="#" class="img-servicio d-block mb-4">
						<img src="static/img/serv_portones.jpg" alt="Image" class="img-servicios-info" class="img-fluid">
					</a>
					<div class="post-text">
						<h3>Portones eléctricos</h3>
						<p>Servicio de automatización de portones para urbanismos o casas, mantenimiento de motor y controles para ello.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="post-entry">
					<a href="#" class="img-servicio d-block mb-4">
						<img src="static/img/serv_motores.jpg" alt="Image" class="img-servicios-info" class="img-fluid">
					</a>
					<div class="post-text">
						<h3>Instalación/mantenimiento motor</h3>
						<p>Suministro e instalación para motores de portones eléctros y santa maría desde instalación hasta mantenimiento (incluye control remoto).</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="post-entry">
					<a href="#" class="img-servicio d-block mb-4">
						<img src="static/img/serv_controles.jpg" alt="Image" class="img-servicios-info" class="img-fluid">
					</a>
					<div class="post-text">
						<h3>Controles remoto</h3>
						<p>Controles remotos para los portones y santa maría.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="post-entry">
					<a href="#" class="img-servicio d-block mb-4">
						<img src="static/img/serv_mantenimiento.jpg" alt="Image" class="img-servicios-info" class="img-fluid">
					</a>
					<div class="post-text">
						<h3>Mantenimiento preventivo y correctivo de computadoras</h3>
						<p>Mantenimiento y actualización de computadoras, respuestos y sistemas operativos.</p>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>