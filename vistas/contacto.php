<section class="hero-section inner-page">
	<div class="wave">
		<svg width="1920px" height="265px" viewBox="0 0 1920 265" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
					<path d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,667 L1017.15166,667 L0,667 L0,439.134243 Z" id="Path"></path>
				</g>
			</g>
		</svg>
	</div>

	<div class="container">
		<div class="row align-items-center">
			<div class="col-12">
				<div class="row justify-content-center">
					<div class="col-md-7 text-center hero-text">
						<h1 data-aos="fade-up" data-aos-delay="">Contáctanos</h1>
						<p class="mb-5" data-aos="fade-up" data-aos-delay="100">Elije la vía más comoda para tí y comunícate con nosotros ¡Estamos para servirte!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="contacto" class="section">
	<div class="container">
		<div class="row mb-5 align-items-end">
			<div class="col-md-6" data-aos="fade-up">
				<h2>Escríbenos</h2>
				<p class="mb-0">Estamos aquí para ayudar en las dudas que tengas o la información que necesites, escríbenos, te respondemos en pocos minutos.</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 ms-auto order-2" data-aos="fade-up">
				<ul class="list-unstyled">
					<li class="mb-3">
						<h5 class="d-block mb-2"><i class="icofont-map"></i> Ubicación</h5>
						<span class="text-black">Barrio Maturin calle 7 entre carreras 7 y 8 Guanare, Portuguesa / Venezuela</span>
					</li>
					<li class="mb-3">
						<h5 class="d-block mb-2"><i class="icofont-smart-phone"></i> Teléfonos</h5>
						<span class="text-black">+58 426-5650727 / +58 426-5514201 / +58 414-5608951</span>
					</li>
					<li class="mb-3">
						<h5 class="d-block mb-2"><i class="icofont-email"></i> Correo electrónico</h5>
						<span class="text-black">wimaxsuperred2020@gmail.com</span>
					</li>
				</ul>
			</div>

			<div class="col-md-6 mb-5 mb-md-0" data-aos="fade-up">
				<form action="forms/contacto.php" method="post" role="form" class="php-email-form">

					<div class="row">
						<div class="col-md-6 form-group">
							<label for="name">Nombre</label>
							<input type="text" name="name" class="form-control" id="name" data-rule="minlen:4" data-msg="Escribe tu nombre" />
							<div class="validate"></div>
						</div>

						<div class="col-md-6 form-group mt-3 mt-md-0">
							<label for="name">Correo electrónico</label>
							<input type="email" class="form-control" name="email" id="email" data-rule="email" data-msg="Escribe un correo válido" />
							<div class="validate"></div>
						</div>

						<div class="col-md-12 form-group mt-3">
							<label for="name">Asunto</label>
							<input type="text" class="form-control" name="subject" id="subject" data-rule="minlen:4" data-msg="Escribe un asunto de tu mensaje" />
							<div class="validate"></div>
						</div>

						<div class="col-md-12 form-group mt-3">
							<label for="name">Mensaje</label>
							<textarea class="form-control" name="message" data-rule="required" data-msg="Escribe el mensaje que deseas comunicar"></textarea>
							<div class="validate"></div>
						</div>

						<div class="col-md-12 mb-3">
							<div class="loading">Cargando</div>
							<div class="error-message"></div>
							<div class="sent-message">Tu mensaje ha sido enviado, gracias por comunicarte</div>
						</div>

						<div class="col-md-6 form-group">
							<input type="submit" class="btn btn-primary d-block w-100" value="Enviar mensaje">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<section id="ubicacion" class="" >
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3940.2006656939866!2d-69.74565638589489!3d9.045452391223279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e7cf5cc790f266f%3A0x98105c6a8b7a795d!2sWimax%20Innovation%20C.A.!5e0!3m2!1ses!2sve!4v1612542202716!5m2!1ses!2sve" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</section>
