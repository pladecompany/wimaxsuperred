<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	//session_start();
	include_once('ruta.php');
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1.0" name="viewport">

		<title>WiMAX SUPER RED</title>
		<meta content="" name="description">
		<meta content="" name="keywords">
		<meta content="" name="author">

		<link href="static/img/favicon.png" rel="icon">
		<link href="static/img/apple-touch-icon.png" rel="apple-touch-icon">

		<link href="static/css/poppins.css" rel="stylesheet">

		<link href="static/css/bootstrap.min.css" rel="stylesheet">
		<link href="static/lib/icofont/icofont.min.css" rel="stylesheet">
		<link href="static/lib/aos/aos.css" rel="stylesheet">
		<link href="static/lib/line-awesome/css/line-awesome.min.css" rel="stylesheet">
		<link href="static/lib/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="static/css/style.css" rel="stylesheet"></head>
		<link rel="stylesheet" href="static\css\flickity.css">
		<script src="static/lib/jquery/jquery.min.js"></script>
	<body>

	<div class="site-mobile-menu site-navbar-target">
		<div class="site-mobile-menu-header">
			<div class="site-mobile-menu-close mt-3">
				<span class="icofont-close js-menu-toggle"></span>
			</div>
		</div>
		<div class="site-mobile-menu-body"></div>
	</div>

	<header class="site-navbar js-sticky-header site-navbar-target" role="banner">

		<div class="container">
			<div class="row align-items-center">
				<div class="col-6 col-lg-2">
					<h1 class="mb-0 site-logo">
					<a href="index.php" class="mb-0"><img id="logo-blanco" class="" src="static/img/logo-blanco.png" width="100%"></a>
					<a href="index.php" class="mb-0"><img id="logo" class="hide" src="static/img/logo.png" width="100%"></a>
					</h1>
				</div>

				<div class="col-12 col-md-10 d-none d-lg-block">
					<nav class="site-navigation position-relative text-end" role="navigation">

						<ul class="site-menu main-menu js-clone-nav me-auto d-none d-lg-block">
							<li class="active"><a href="index.php?op=inicio" class="nav-link">Inicio</a></li>
							<li><a href="index.php?op=servicios" class="nav-link">Servicios</a></li>
							<li><a href="index.php?#testimonios" class="nav-link">Testimonios</a></li>
							<li><a href="index.php?op=contacto" class="nav-link">Contacto</a></li>
							<a href="index.php?op=fibra_optica"  class=" display-inline-block btn btn-primary "> Fibra óptica</a>
						</ul>
					</nav>
				</div>

				<div class="col-6 d-inline-block d-lg-none ms-md-0 py-3" style="position: relative; top: 3px;">
					<a href="#" class="burger site-menu-toggle js-menu-toggle" data-bs-toggle="collapse" data-bs-target="#main-navbar">
						<span></span>
					</a>
				</div>
			</div>
		</div>
	</header>


	<main id="main">
		<?php include_once($ruta); ?>
	</main>

	<section class="section cta-section p-0 ">
		<div class="container">
			<div class="row pt-5 pb-5 align-items-center">

			<div class="col-md-6 me-auto text-center text-md-start mb-5 mb-md-0">
				<h2>¡Síguenos en Instagram!</h2>
				<a href="https://instagram.com/wimaxsuperred?igshid=cac61rxmske6" target="_blank" ><p class="h4 text-white" >@wimaxsuperred</p></a>
			</div>
			<div class="col-md-6 me-auto text-center text-md-start mb-5 mb-md-0">
					<script src="https://apps.elfsight.com/p/platform.js" defer></script>
					<div class="elfsight-app-6f2cbd64-6550-4dea-a63f-891411cdfae1"></div>
				</div>
		</div>
	</section>

	<footer class="footer" role="contentinfo">
		<div class="container">
			<div class="row">
				<div class="col-md-4 mb-4 mb-md-0">
					<h3>WIMAX SUPER RED</h3>
				</div>

				<div class="col-md-7 ms-auto">
					<div class="row site-section pt-0">
						<div class="col-md-6 mb-4 mb-md-0">
							<h3 >Servicios</h3>
							<ul class="list-unstyled">
								<li><a href="index.php?op=inicio#servicios">Servicio de internet</a></li>
								<li><a href="index.php?op=inicio#servicios">Circuito cerrado CCTV</a></li>
								<li><a href="index.php?op=inicio#servicios">Cercado eléctrico</a></li>
								<li><a href="index.php?op=inicio#servicios">Instalación de alarmas</a></li>
								<li><a href="index.php?op=inicio#servicios">Adicionales</a></li>
							</ul>
						</div>

						<div class="col-md-6 mb-4 mb-md-0">
							<h3>Nosotros</h3>
							<ul class="list-unstyled">
								<li><a href="index.php?op=inicio#nosotros">Acerca de nosotros</a></li>
								<li><a href="#">Presupuesto</a></li>
								<li><a href="index.php?op=contacto#ubicacion">Ubicación</a></li>
								<li><a href="index.php?op=contacto#contacto">Contacto</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="row justify-content-center text-center">
				<div class="col-md-7">
					<p class="copyright">&copy; 2021. Todos los derechos reservados, desarrollado por <a href="">Plade Company C.A</a></p>
				</div>
			</div>
		</div>
	</footer>

	<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

	<script src="static/js/logo-scroll.js"></script>

	<script src="static/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="static/lib/jquery.easing/jquery.easing.min.js"></script>
	<script src="static/lib/php-email-form/validate.js"></script>
	<script src="static/lib/aos/aos.js"></script>
	<script src="static/lib/owl.carousel/owl.carousel.min.js"></script>
	<script src="static/lib/jquery-sticky/jquery.sticky.js"></script>
    <script src="static\js\flickity.js"></script>
	<script src="static/js/main.js"></script>

</body>

</html>